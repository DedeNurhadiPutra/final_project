@extends('layouts.master')
@section('title')
    Tambah Peminjaman
@endsection

@section('content')
<div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/loans" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <select id="member_id" class="form-control" name="member_id" required>
                            <option value="">Pilih Member...</option>
                        @foreach ($members as $member)
                            @if (old('member_id') == $member->id)
                                <option selected value="{{ $member->id }}">{{ $member->name }}</option>
                            @else
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('member_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <select id="book_id" class="form-control" name="book_id" required>
                        <option value="">Pilih Buku...</option>
                    @foreach ($books as $book)
                        @if ($book->stock <= 0)
                            <option disabled>{{$book->title}} - {{$book->writer}} </option>
                        @else
                            <option value="{{$book->id}}">{{$book->title}} - {{$book->writer}} </option>
                        @endif
                    @endforeach
                    </select>
                    @error('book_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="duration">Lama Peminjaman</label>
                        <input type="number" class="form-control" id="duration" name="duration" placeholder="Masukkan Lama Peminjaman" value="{{ old('duration', '') }}">
                        @error('duration')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
            </div>
            <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah Data</button>
        </div>
    </form>
    </div>
    <!-- /.card -->
</div>
@endsection