@extends('layouts.master')
@section('title')
    Data Peminjaman
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<div class="container-fluid">
  <div class="card">
      <div class="card-header">
          <h3 class="card-title">Berikut adalah list {{$title}}</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
          @if (session('success'))
              <div class="alert alert-success" role="alert">
                  {{session('success')}}
              </div>
          @endif
      <a href="/loans/create" class="btn btn-primary mb-2">Tambah Peminjaman</a>
      <a href="/print/loans" class="btn btn-secondary mb-2">Print Data</a>
      <table id="example1" class="table table-bordered table-striped">
        <thead class="thead-light">
          <tr>
            <th>No</th>
            <th>Kode Buku</th>
            <th>Nama Member</th>
            <th>Tanggal Peminjaman</th>
            <th>Lama Peminjaman</th>
            <th>Denda</th>
            <th>Total Biaya</th>
            <th>Status</th>
            <th>Admin</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($loans as $key => $loan)
          <tr>
              <td>{{$key + 1}}</td>
              {{-- <td>{{$loan->books->code_book}}</td>
              <td>{{$loan->members->name}}</td> --}}
              <td>{{$loan->loan_date}}</td>
              <td>{{$loan->duration}}</td>
              <td>{{$loan->late_charge}}</td>
              {{-- <td>{{$loan->book_id->price}}</td> --}}
              <td>{{$loan->status}}</td>
              {{-- <td>{{$loan->admin->name}}</td> --}}
              <td style="display: flex; justify-content: space-evenly;">
                  <a href="/loans/{{$loan->id}}" class="btn btn-info btn-sm">Show</a>
                  <a href="/loans/{{$loan->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                  <form action="/loans/{{$loan->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <input type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')" value="Delete">
                  </form>
              </td>
          </tr>
      @empty
          <tr>
              <td colspan="6" align="center">No data</td>
          </tr>  
      @endforelse              
        </tbody>
      </table>
    </div>
  </div>
  </div>
</div>
<!-- /.container-fluid -->
    @endsection