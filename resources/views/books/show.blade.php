@extends('layouts.master')
@section('title')
    Detail Buku {{$book->title}}
@endsection

@section('content')

<div class="card" style="width: 18rem;"></div>
    <img src="{{ asset('img/'.$book->cover) }}" class="card-img-top" alt="..." style="width:400px">
    <div class="card-body">
        <h4 class="card-title">{{$book->title}}</h4>
        <h5 class="card-text">{{$book->code_book}}</h5>
        <p class="card-text">{{$book->writer}}</p>
        <p class="card-text">{{$book->stock}}</p>
        <p class="card-text">{{$book->category->name}}</p>
        <p class="card-text">{{$book->price}}</p>
        <p class="card-text">{{$book->admin->name}}</p>
    </div>
</div>

@endsection