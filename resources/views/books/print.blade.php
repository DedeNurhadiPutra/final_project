<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
</head>
<body>
    <div>
        <h1 style="margin-top:50px" align="center">Data Buku</h1>
        <table border="1" align="center" cellpadding="10" cellspacing="0">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Kode Buku</th>
                <th scope="col">Judul</th>
                <th scope="col">Penulis</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Kategori</th>
                <th scope="col">Harga</th>
            </tr>
            @foreach ($books as $key => $book)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$book->code_book}}</td>
                    <td>{{$book->title}}</td>
                    <td>{{$book->writer}}</td>
                    <td>{{$book->stock}}</td>
                    <td>{{$book->category->name}}</td>
                    <td>{{$book->price}}</td>
                </tr>
            @endforeach
        </table>
    </div>
</body>
</html>