@extends('layouts.master')
@section('title')
    Tambah Buku
@endsection

@section('content')
<div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/books" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="cover">Cover</label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="cover">Pilih Cover Image</label>
                            <img class="img-preview img-fluid mb-3 col-sm-5">
                            <input type="file" class="custom-file-input" id="cover" name="cover" onchange="previewImage()">
                        </div>
                    @error('cover')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="code_book">Kode Buku</label>
                    <input type="number" class="form-control" id="code_book" name="code_book" placeholder="Masukkan No.HP Member" value="{{ old('code_book', '') }}">
                    @error('code_book')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Masukkan No.HP Member" value="{{ old('title', '') }}">
                    @error('title')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="writer">Penulis</label>
                    <input type="text" class="form-control" id="writer" name="writer" placeholder="Masukkan No.HP Member" value="{{ old('writer', '') }}">
                    @error('writer')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="year">Tahun Terbit</label>
                    <input type="number" class="form-control" id="year" name="year" placeholder="Masukkan No.HP Member" value="{{ old('year', '') }}">
                    @error('year')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="stock">Jumlah</label>
                    <input type="number" class="form-control" id="stock" name="stock" placeholder="Masukkan No.HP Member" value="{{ old('stock', '') }}">
                    @error('stock')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <select class="form-control select2" name="category_id">
                        <option selected="" disabled="">Pilih Kategori</option>
                        @foreach($categories as $category)
                            @if (old('category_id') == $category->id)
                                <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('category_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="price">Harga</label>
                    <input type="number" class="form-control" id="price" name="price" placeholder="Masukkan No.HP Member" value="{{ old('price', '') }}">
                    @error('price')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah Data</button>
        </div>
        </form>
    </div>
    <!-- /.card -->
</div>
@endsection

@push('scripts')
    <script>
        function previewImage(){
            const cover = document.querySelector('#cover');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader;
            oFReader.readAsDataURL(cover.files[0]);

            oFReader.onload = function(oFREvent){
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endpush
