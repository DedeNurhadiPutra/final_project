@extends('layouts.master')
@section('title')
    Data Member
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Berikut adalah list {{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            @endif
        <a href="/members/create" class="btn btn-primary mb-2">Tambah Data</a>
        <a href="/print/members" class="btn btn-secondary mb-2">Print Data</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Created by</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($members as $key => $member)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$member->name}}</td>
                        <td>{{$member->address}}</td>
                        <td>{{$member->phone}}</td>
                        <td>{{$member->admin->name}}</td>
                        <td style="display: flex; justify-content: space-evenly;">
                            <a href="/members/{{$member->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/members/{{$member->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/members/{{$member->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" align="center">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{asset('adminlte')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush