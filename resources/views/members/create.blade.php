@extends('layouts.master')
@section('title')
    Tambah Member
@endsection

@section('content')
<div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/members" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Member</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama Member" value="{{ old('name', '') }}">
                    @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="address">Alamat Member</label>
                    <br>
                    <textarea name="address" id="address" cols="30" rows="10" class="form-control">{{ old('address', '') }}</textarea>
                    @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="phone">No. Handphone</label>
                    <input type="number" class="form-control" id="phone" name="phone" placeholder="Masukkan No.HP Member" value="{{ old('phone', '') }}">
                    @error('phone')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah Data</button>
        </div>
        </form>
    </div>
    <!-- /.card -->
</div>

@endsection