@extends('layouts.master')
@section('title')
    Detail Member {{$member->name}}
@endsection

@section('content')

<div class="card" style="width: 18rem;">
    <div class="card-body">
        <h4 class="card-title">{{$member->name}}</h4>
        <h5 class="card-text">{{$member->phone}}</h5>
        <p class="card-text">{{$member->address}}</p>
        <a href="/members" class="card-link">Kembali</a>
    </div>
</div>

@endsection