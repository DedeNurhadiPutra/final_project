<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
</head>
<body>
    <div>
        <h1 style="margin-top:50px" align="center">Data Member</h1>
        <table border="1" align="center" cellpadding="10" cellspacing="0">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Address</th>
                <th scope="col">Phone</th>
            </tr>
            @foreach ($members as $key => $member)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$member->name}}</td>
                    <td>{{$member->address}}</td>
                    <td>{{$member->phone}}</td>
                </tr>
            @endforeach
        </table>
    </div>
</body>
</html>