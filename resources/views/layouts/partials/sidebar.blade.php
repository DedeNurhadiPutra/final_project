<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
        <img src="{{asset('adminlte')}}/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
        <a href="#" class="d-block">{{Auth::user()->name}}</a>
    </div>
    </div>

    <!-- SidebarSearch Form -->

    <!-- Sidebar Menu -->
    <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
            <li class="nav-header">DATA</li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-table"></i>
                    <p>
                        User
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="/admin" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Admin</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="/members" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Member</p>
                </a>
                </li>
            </ul>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-table"></i>
                    <p>
                        Buku
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="/categories" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kategori</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="/books" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Buku</p>
                </a>
                </li>
            </ul>
            
            <li class="nav-header">TRANSAKSI</li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    Peminjaman
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/loans/create" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tambah Data Pinjaman</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/loans" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>List Data Pinjaman</p>
                    </a>
                  </li>
                </ul>
              </li>
        </li>
    </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>