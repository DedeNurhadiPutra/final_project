@extends('layouts.master')
@section('title')
    Data Kategori
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Berikut adalah list {{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            @endif
        <a href="/categories/create" class="btn btn-primary mb-2">Tambah Kategori</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama Kategori</th>
                    <th scope="col">Dibuat oleh</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($categories as $key => $category)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$category->name}}</td>
                        <td>{{$category->admin->name}}</td>
                        <td style="display: flex; justify-content: space-evenly;">
                            <a href="/categories/{{$category->id}}" class="btn btn-info btn-sm">Tampilkan</a>
                            <a href="/categories/{{$category->id}}/edit" class="btn btn-primary btn-sm">Ubah</a>
                            <form action="/categories/{{$category->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin?')" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" align="center">Data tidak ditemukan</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{asset('adminlte')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush