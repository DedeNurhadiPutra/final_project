@extends('layouts.master')
@section('title')
    Tambah Kategori
@endsection

@section('content')
<div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{$title}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/categories" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Kategori</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama Kategori" value="{{ old('name', '') }}">
                    @error('name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah Kategori</button>
        </div>
        </form>
    </div>
    <!-- /.card -->
</div>

@endsection