@extends('layouts.master')
@section('title')
    Detail Category {{$category->name}}
@endsection

@section('content')

<div class="card" style="width: 18rem;">
    <div class="card-body">
        <h4 class="card-title">{{$category->name}}</h4>
        <h5 class="card-text">{{$category->created_at}}</h5>
        <p class="card-text">{{$category->admin->name}}</p>
        <a href="/categories" class="card-link">Kembali</a>
    </div>
</div>

@endsection