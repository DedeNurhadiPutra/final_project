@extends('layouts.master')
@section('title')
    Data Admin Perpustakaan
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
    
@section('content')    
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Berikut adalah data {{$title}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Terdaftar tanggal</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($users as $key => $user)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" align="center"> Data Tidak Ditemukan. </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{asset('adminlte')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
