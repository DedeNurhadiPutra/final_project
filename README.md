# Final Project

## Kelompok 9

## Anggota Kelompok :
- Dede Nurhadi Putra
- Ajib Wicaksono
- Alvi Huda

## Tema Project :
Perpustakaan

## ERD :
<img src="public/img/final_project_erd.png" width="400">

## Link :
Link Demo Project   : https://youtu.be/i9fH6Ui5Ri0

Link Deploy Heroku  : https://final-project-laravel-perpus.herokuapp.com