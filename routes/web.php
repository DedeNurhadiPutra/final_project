<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// Category
Route::resource('categories', 'CategoryController');

// Books
Route::resource('books', 'BookController');
Route::get('/print/books', 'BookController@print');

// Admin
Route::get('/admin', 'UserController@index');

// Members
Route::resource('members', 'MemberController');
Route::get('/print/members', 'MemberController@print');

// Loans
Route::resource('loans', 'LoanController');
