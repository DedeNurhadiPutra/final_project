<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $table    = 'categories';
    protected $guarded = [];

    public function admin() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function books() {
        return $this->hasMany('App\Book');
    }
}
