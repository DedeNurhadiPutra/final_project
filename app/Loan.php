<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model {
    protected $guarded = [];

    public function books() {
        return $this->belongsToMany('App\Book');
    }
    public function members() {
        return $this->belongsToMany('App\Member');
    }
    public function admin() {
        return $this->belongsToMany('App\User');
    }
    public function category() {
        return $this->belongsTo('App\Category');
    }
}
