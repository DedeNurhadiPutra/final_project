<?php

namespace App\Http\Controllers;

use App\Loan;
use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Category;
use App\Member;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade as PDF;

class LoanController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title = 'Data Peminjaman';
        $loans = Loan::all();

        return view('loans.index', compact('title', 'loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $title      = 'Form Data Peminjaman Baru';
        $members    = Member::all();
        $books      = Book::all();


        return view('loans.create', compact('title', 'members', 'books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'member_id'  => 'required',
            'book_id' => 'required',
        ]);

        $store = Loan::create([
            'member_id' => $request['member_id'],
            'book_id'   => $request['book_id'],
            'status'    => 'Belum Dikembalikan',
            'loan_date' => date('Y-m-d H:i:s'),
            'duration'  => $request['duration'],
            'late_charge' => 0,
            'total'     => $request['book_id'],
            'user_id'   => Auth::id()
        ]);

        Alert::success('Success', 'Member Berhasil ditambahkan');

        return redirect('/loans');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
