<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Member;
use Illuminate\Support\Facades\Auth;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade as PDF;


class MemberController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title   = 'Data Member';
        $members = Member::all();

        return view('members.index', compact('title', 'members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $title  = 'Form Data Member Baru';

        return view('members.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric|digits_between:12,13'
        ]);

        $store = Member::create([
            'name'     => $request['name'],
            'address'  => $request['address'],
            'phone'    => $request['phone'],
            'user_id'  => Auth::id()
        ]);

        Alert::success('Success', 'Member Berhasil ditambahkan');

        return redirect('/members');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $member = Member::findOrFail($id);

        return view('members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $title = 'Edit Data Member';
        $member   = Member::find($id);

        return view('members.edit', compact('title', 'member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request) {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric|digits_between:12,13'
        ]);


        $update = Member::where('id', $id)->update([
            'name'     => $request['name'],
            'address'  => $request['address'],
            'phone'    => $request['phone'],
            'user_id'  => Auth::id()
        ]);

        Alert::success('Success', 'Berhasil Update Member');
        return redirect('/members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Member::destroy($id);

        Alert::success('Success', 'Berhasil Hapus Member');
        return redirect('/members');
    }

    public function print() {
        $title      = 'Print Data Member';
        $members    = Member::all();
        $pdf        = PDF::loadView('members.print', [
            'title'     => $title,
            'members'   => $members
        ]);

        return $pdf->stream();
    }
}
