<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Book;
use App\User;
use App\Category;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;

class CategoryController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title   = 'Data Kategori';
        $categories = Category::all();

        return view('categories.index', compact('title', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $title  = 'Form Tambah Kategori';

        return view('categories.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'name' => 'required|unique:App\Category,name'
        ]);

        $store = Category::create([
            'name'     => $request['name'],
            'user_id'  => Auth::id()
        ]);

        Alert::success('Success', 'Berhasil Menambahkan Category');

        return redirect('/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $category = Category::find($id);

        return view('categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $title = 'Edit Kategori';
        $category   = Category::find($id);

        return view('categories.edit', compact('title', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request) {
        $request->validate([
            'name' => 'required'
        ]);


        $update = Category::where('id', $id)->update([
            'name'     => $request['name'],
            'user_id'  => Auth::id()
        ]);

        Alert::success('Success', 'Berhasil Ubah Kategori');
        return redirect('/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Category::destroy($id);

        Alert::success('Success', 'Berhasil Hapus Kategori');
        return redirect('/categories');
    }
}
