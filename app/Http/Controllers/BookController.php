<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Category;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade as PDF;
use File;

class BookController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title = 'Data Buku';
        $books = Book::all();

        return view('books.index', compact('title', 'books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $title      = 'Form Tambah Buku';
        $categories = Category::all();

        return view('books.create', compact('title', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'cover'     => 'nullable|image|file|max:1024',
            'code_book' => 'required|unique:App\Book,code_book|digits_between:4,6',
            'title'     => 'required',
            'writer'    => 'required',
            'year'      => 'required',
            'stock'     => 'required',
            'price'     => 'required'
        ]);

        $coverName = time() . '.' . $request->cover->extension();

        $request->cover->move(public_path('img'), $coverName);

        $store = Book::create([
            'cover'     => $coverName,
            'code_book' => $request['code_book'],
            'title'     => $request['title'],
            'writer'    => $request['writer'],
            'year'      => $request['year'],
            'stock'     => $request['stock'],
            'category_id' => $request['category_id'],
            'price'     => $request['price'],
            'user_id'   => Auth::id()
        ]);

        Alert::success('Success', 'Berhasil Menambahkan Buku');

        return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $book = Book::find($id);

        return view('books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $title = 'Edit Buku';
        $book   = Book::find($id);
        $categories = Category::all();

        return view('books.edit', compact('title', 'book', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $request->validate([
            'cover'     => 'nullable|image|file|max:1024',
            'code_book' => 'required|digits_between:4,6',
            'title'     => 'required',
            'writer'    => 'required',
            'year'      => 'required',
            'stock'     => 'required',
            'price'     => 'required'
        ]);

        if ($request->has('cover')) {
            $coverName = time() . '.' . $request->cover->extension();

            $request->cover->move(public_path('img'), $coverName);

            Book::where('id', $id)->update([
                'cover'     => $coverName,
                'code_book' => $request['code_book'],
                'title'     => $request['title'],
                'writer'    => $request['writer'],
                'year'      => $request['year'],
                'stock'     => $request['stock'],
                'category_id' => $request['category_id'],
                'price'     => $request['price'],
                'user_id'   => Auth::id()
            ]);
        } else {
            Book::where('id', $id)->update([
                'code_book' => $request['code_book'],
                'title'     => $request['title'],
                'writer'    => $request['writer'],
                'year'      => $request['year'],
                'stock'     => $request['stock'],
                'category_id' => $request['category_id'],
                'price'     => $request['price'],
                'user_id'   => Auth::id()
            ]);
        }

        Alert::success('Success', 'Berhasil Ubah Buku');
        return redirect('/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $book = Book::find($id);
        $path = "img/";

        File::delete($path . $book->cover);
        Book::destroy($id);

        Alert::success('Success', 'Berhasil Hapus Kategori');
        return redirect('/books');
    }

    public function print() {
        $title      = 'Print Data Buku';
        $books      = Book::all();
        $pdf        = PDF::loadView('books.print', [
            'title'     => $title,
            'books'   => $books
        ]);

        return $pdf->stream();
    }
}
